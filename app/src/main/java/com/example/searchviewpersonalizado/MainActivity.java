package com.example.searchviewpersonalizado;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView lst;
    private SearchView srcLista;
    private ArrayAdapter<ItemData> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<ItemData> list = new ArrayList<>();
        list.add(new ItemData(getString(R.string.itemFrappses), getString(R.string.msgFrapsses), R.drawable.g2));
        list.add(new ItemData(getString(R.string.itemAgradecimiento), getString(R.string.msgAgradecimiento), R.drawable.nrg));
        list.add(new ItemData(getString(R.string.itemAmor), getString(R.string.msgAmor), R.drawable.pk));
        list.add(new ItemData(getString(R.string.itemNewyear), getString(R.string.msgNewyear), R.drawable.ssg));
        list.add(new ItemData(getString(R.string.itemCanciones), getString(R.string.msgCanciones), R.drawable.rogue));
        list.add(new ItemData(getString(R.string.Itempsg), getString(R.string.msgpsg), R.drawable.c9));
        lst = (ListView) findViewById(R.id.lstNombres);
        srcLista = (SearchView) findViewById(R.id.menu_search);
        adapter = new ListviewAdapter(this,R.layout.activity_search_adapter,R.id.lblCategorias,list);
        lst.setAdapter(adapter);

        lst.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(parent.getContext(), getString(R.string.msgSeleccionado) +" "+((ItemData)parent.getItemAtPosition(position)).getTextCategoria(),Toast.LENGTH_SHORT).show();
            }
        });
    }
    public boolean onCreateOptionsMenu (Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.buscador1, menu);

        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String q) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.getFilter().filter(s);
                return false;
            }
        });
        return  super.onCreateOptionsMenu(menu);
    }
}