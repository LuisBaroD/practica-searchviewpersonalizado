package com.example.searchviewpersonalizado;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class ListviewAdapter extends ArrayAdapter<ItemData> implements Filterable{
    int groupId;
    Activity Context;
    ArrayList<ItemData> list;
    LayoutInflater inflater;
    private List<ItemData> ejemploListaLlena;

    public ListviewAdapter(Activity Context, int groupId, int id, ArrayList<ItemData>list){
        super(Context,id,list);
        this.list = list;
        inflater = (LayoutInflater)Context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.groupId = groupId;
        ejemploListaLlena = new ArrayList<>(list);
    }
    public View getView(int posicion, View convertView, ViewGroup parent){

        View itemView = inflater.inflate(groupId,parent,false);

        ImageView imagen = (ImageView) itemView.findViewById(R.id.imgCategoria);
        imagen.setImageResource(list.get(posicion).getImageId());

        TextView textCategoria = (TextView) itemView.findViewById(R.id.lblCategorias);
        textCategoria.setText(list.get(posicion).getTextCategoria());
        TextView textDescripcion = (TextView) itemView.findViewById(R.id.lblDescripcion);
        textDescripcion.setText(list.get(posicion).getTextDescripcion());

        return itemView;
    }
    public View getDropDownView(int posicion,View convertView,ViewGroup parent){
        return getView(posicion,convertView,parent);
    }

    public Filter getFilter(){
        return ejemploFiltro;
    }
    private Filter ejemploFiltro = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<ItemData> ListaFiltrada = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                ListaFiltrada.addAll(ejemploListaLlena);
            } else {
                String patronFiltro = constraint.toString().toLowerCase().trim();
                for (ItemData item : ejemploListaLlena) {
                   if (item.getTextCategoria().toLowerCase().contains(patronFiltro)){
                       ListaFiltrada.add(item);
                   }
                }
            }

            FilterResults results = new FilterResults();
            results.values = ListaFiltrada;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            list.clear();
            list.addAll((List)results.values);
            notifyDataSetChanged();

        }
    };
}
